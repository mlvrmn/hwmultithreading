﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace HWParallelTaskThread
{
    class Program
    {
        public Int64 ThreadTest(Int64[] array)
        {
            var stopWatch = new Stopwatch();

            Int64 sum = 0;

            stopWatch.Start();

            foreach (Int64 element in array)
                sum += element;

            stopWatch.Stop();

            return stopWatch.ElapsedMilliseconds;
        }



        static void Main(string[] args)
        {
            Program program = new Program();

            Console.WriteLine("Введите количество элементов массива до 1000000000 : ");

            Int64 count = 0;
            Boolean b = Int64.TryParse(Console.ReadLine(), out count);


            Int64[] array = new Int64[count];

            for (Int32 i = 0; i < array.Length; i++)
            {
                array[i] = i;
            }


            Console.WriteLine("Cуммирование {0} элементов массива занимает {1} мс." +
                " времени при вычислении в одном потоке", count, program.ThreadTest(array).ToString());


            ///////////////////////////////////////////////////////////////////////////////////////////

            Stopwatch stopwatch = new Stopwatch();

            Int64 sum = 0;
            stopwatch.Start();
            ParallelLoopResult result = Parallel.ForEach(array, (numb) =>
            {
                sum += numb;
            });
            stopwatch.Stop();

            Console.WriteLine("Cуммирование {0} элементов массива занимает {1} мс. при паралельном выполнении цикла",
                                count, stopwatch.ElapsedMilliseconds);

            sum = 0;
            stopwatch.Reset();
            ////////////////////////////////////////////////////////////////////////////////////////////


            stopwatch.Start();
            sum = array.AsParallel().Sum(x => x);
            stopwatch.Stop();
            Console.WriteLine("Cуммирование {0} элементов массива занимает {1} мс. при использовании PLing",
                count, stopwatch.ElapsedMilliseconds);


            sum = 0;
            stopwatch.Reset();

            Console.ReadKey();
            Console.Clear();
        }
    }
}
